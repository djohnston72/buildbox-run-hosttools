What is buildbox-run-hosttools?
===============================

``buildbox-run-hosttools`` attempts to run actions without any sandboxing.

Usage
=====
::

    usage: ./buildbox-run-hosttools [OPTIONS]
        --action=PATH               Path to read input Action from
        --action-result=PATH        Path to write output ActionResult to
        --log-level=LEVEL           (default: info) Log verbosity: trace/debug/info/warning/error
        --verbose                   Set log level to debug
        --log-directory=DIR         Write logs to this directory with filenames:
                                    buildbox-run-hosttools.<hostname>.<user name>.log.<severity level>.<date>.<time>.<pid>
        --use-localcas              Use LocalCAS protocol methods (default behavior)
                                    NOTE: this option will be deprecated.
        --disable-localcas          Do not use LocalCAS protocol methods
        --workspace-path=PATH       Location on disk which runner will use as root when executing jobs
        --stdout-file=FILE          File to redirect the command's stdout to
        --stderr-file=FILE          File to redirect the command's stderr to
        --no-logs-capture           Do not capture and upload the contents written to stdout and stderr
        --capabilities              Print capabilities supported by this runner
        --validate-parameters       Only check whether all the required parameters are being passed and that no
                                    unknown options are given. Exits with a status code containing the result (0 if successful).
        --remote=URL                URL for CAS service
        --instance=NAME             Name of the CAS instance
        --server-cert=PATH          Public server certificate for TLS (PEM-encoded)
        --client-key=PATH           Private client key for TLS (PEM-encoded)
        --client-cert=PATH          Public client certificate for TLS (PEM-encoded)
        --access-token=PATH         Access Token for authentication (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token.
        --token-reload-interval=MINUTES Time to wait before refreshing access token from disk again. The following suffixes can be optionally specified: M (minutes), H (hours). Value defaults to minutes if suffix not specified.
        --googleapi-auth            Use GoogleAPIAuth when this flag is set.
        --retry-limit=INT           Number of times to retry on grpc errors
        --retry-delay=MILLISECONDS  How long to wait before the first grpc retry
