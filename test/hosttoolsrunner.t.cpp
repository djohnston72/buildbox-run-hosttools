/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_hosttools.h>

#include <gtest/gtest.h>

using namespace buildboxcommon;
using namespace buildboxrun;
using namespace hosttools;

TEST(PrefixArgumentTests, TestCustomParser)
{
    HostToolsRunner runner;
    EXPECT_TRUE(runner.parseArg("--prefix-staged-dir"));
    EXPECT_FALSE(runner.parseArg("--some-other-argument"));
    EXPECT_FALSE(runner.parseArg("--verbose"));
}
